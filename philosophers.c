#define N 5
#define LEFT ( i + N - 1 ) % N
#define RIGHT ( i + 1 ) % N

#define THINKING 0
#define HUNGRY 1
#define EATING -1

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

pthread_mutex_t m;	// initialized to 1
int	state[N];	// initiated to THINKING's
pthread_mutex_t s[N];	//initialized to 0's
pthread_t tid[N];
int eaten[N];

void test(int i) {

	if( state[i] == HUNGRY
		&& state[LEFT] != EATING
		&& state[RIGHT] != EATING )
	{
		state[i] = EATING;
		pthread_mutex_unlock(&s[i]);
	}
}

void grab_forks(int i) {

	pthread_mutex_lock(&m);
	state[i] = HUNGRY;
	test(i);
	pthread_mutex_unlock(&m);
	pthread_mutex_lock(&s[i]);
}

void put_away_forks(int i)
{
	pthread_mutex_lock(&m);
		state[i] = THINKING;
		test( LEFT );
		test( RIGHT );
	pthread_mutex_unlock(&m);
}

void* philosopher(void* i) {

	printf("%d", *(int*)i);

	while(1) {
	
		put_away_forks(*(int*)i);
		sleep(1);	
		eaten[*(int*)i]++;
		printf("Philosopher: %d ate %d times\n", *(int*)i, eaten[*(int*)i]);
		fflush(stdout);
		grab_forks(*(int*)i);
		sleep(1);
	}

	return NULL;
}

int main() {

	pthread_mutex_init(&m, NULL);
	pthread_mutex_unlock(&m);
	
	for(int i = 0; i < N; i++) {

		int* arg = malloc(sizeof(int));
		*arg = i;

		pthread_mutex_init(&s[i], NULL);
		pthread_mutex_lock(&s[i]);
		int t_status = pthread_create(&(tid[i]), NULL, &philosopher, (void*)arg);
		if(t_status != 0) {
			printf("Error: philosopher: %d creation failure", i + 1);
			fflush(stdout);
		}		
	}

	for(int i = 0; i < N; i++) {
		pthread_join(tid[i], NULL);
	}

	pthread_mutex_destroy(&m);
	pthread_mutex_destroy(&s[N]);
}
